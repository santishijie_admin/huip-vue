import HuipLayout from './src/layout'

/* istanbul ignore next */
HuipLayout.install = function (Vue) {
  Vue.component(HuipLayout.name, HuipLayout)
}

export default HuipLayout
