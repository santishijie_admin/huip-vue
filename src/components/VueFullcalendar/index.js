import VueFullcalendar from './packages/calendar/index.js'
import VueDraggable from './packages/draggable/index.js'

const components = [
  VueFullcalendar,
  VueDraggable
]

const install = function (Vue) {
  components.map(component => {
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  version: '1.0.1',
  name: 'VueFullcalendar',
  install,
  VueFullcalendar,
  VueDraggable
}
