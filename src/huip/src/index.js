import HuipReservation from '../packages/reservation/index.js'

const components = [
  HuipReservation.ReservationDeviceEdit,
  HuipReservation.ReservationProjectEdit,
  HuipReservation.ReservationInspectEdit,
  HuipReservation.ReservationInspectTypeEdit
]

const install = function (Vue) {
  components.map(component => {
    Vue.component(component.name, component)
  })
}

if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue)
}

export default {
  version: '1.0.1',
  name: 'Huip',
  install,
  ReservationDeviceEdit: HuipReservation.ReservationDeviceEdit,
  ReservationProjectEdit: HuipReservation.ReservationProjectEdit,
  ReservationInspectEdit: HuipReservation.ReservationInspectEdit,
  ReservationInspectTypeEdit: HuipReservation.ReservationInspectTypeEdit
}
