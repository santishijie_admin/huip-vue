// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from '@/components/App'
import router from './router'
import store from '@/store'
import adminStore from 'admin/store'
import ElementUI from 'element-ui'
import Huip from '@/huip/src/index'
import 'element-ui/lib/theme-chalk/index.css'
// import '@/mock'

Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.use(Huip)
store.registerModule('adminStore', adminStore)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
