import index from 'admin/pages/index'
const Wrap = { template: '<router-view></router-view>' }

const routers = [{
  path: '/',
  name: 'index',
  text: '管理台',
  redirect: '/dashboard',
  component: index,
  children: [{
    path: '/dashboard',
    name: 'dashboard',
    text: '仪表盘',
    menu: true,
    icon: 'fas fa-tachometer-alt',
    component: function (resolve) {
      require(['admin/pages/Dashboard'], resolve)
    }
  }, {
    path: '/user',
    name: 'user',
    text: '用户管理',
    menu: true,
    icon: 'fas fa-user-cog',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/user/client',
      name: 'user_client',
      text: '客户端管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/user/client'], resolve)
      }
    }, {
      path: '/user/org',
      name: 'user_org',
      text: '组织机构',
      menu: true,
      component: Wrap,
      hasChildren: true,
      children: [{
        path: '/user/org/org',
        name: 'user_org_org',
        text: '组织机构',
        menu: true,
        component: function (resolve) {
          require(['admin/pages/user/org'], resolve)
        }
      }, {
        path: '/user/org/type',
        name: 'user_org_type',
        text: '组织类型',
        menu: true,
        component: function (resolve) {
          require(['admin/pages/user/orgType'], resolve)
        }
      }]
    }, {
      path: '/user/role',
      name: 'user_role',
      text: '角色管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/user/role'], resolve)
      }
    }, {
      path: '/user/auth',
      name: 'user_auth',
      text: '权限管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/user/auth'], resolve)
      }
    }, {
      path: '/user/user',
      name: 'user_user',
      text: '用户管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/user/user'], resolve)
      }
    }]
  }, {
    path: '/reservation',
    name: 'reservation',
    text: '预约系统',
    menu: true,
    icon: 'fas fa-calendar-check',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/reservation/device',
      name: 'reservation_device',
      text: '设备管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/reservation/device'], resolve)
      }
    }, {
      path: '/reservation/it',
      name: 'reservation_it',
      text: '检查类别',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/reservation/inspect-type'], resolve)
      }
    }, {
      path: '/reservation/project',
      name: 'reservation_project',
      text: '项目管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/reservation/project'], resolve)
      }
    }]
  }, {
    path: '/dictionary',
    name: 'dictionary',
    text: '数据仓库',
    menu: true,
    icon: 'fas fa-database',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/dictionary/diagnosis',
      name: 'dictionary_diagnosis',
      text: '诊断库',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/dictionary/diagnosis'], resolve)
      }
    }, {
      path: '/dictionary/operating',
      name: 'dictionary_operating',
      text: '术式库',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/dictionary/operating'], resolve)
      }
    }, {
      path: '/dictionary/subject',
      name: 'dictionary_subject',
      text: '学科',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/dictionary/subject'], resolve)
      }
    }, {
      path: '/dictionary/part',
      name: 'dictionary_part',
      text: '人体部位',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/dictionary/part'], resolve)
      }
    }, {
      path: '/dictionary/symptom',
      name: 'dictionary_symptom',
      text: '症状管理',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/dictionary/symptom'], resolve)
      }
    }]
  }, {
    path: '/message',
    name: 'message',
    text: '消息管理',
    menu: true,
    icon: 'far fa-envelope',
    component: Wrap,
    hasChildren: true,
    children: [{
      path: '/message/type',
      name: 'message_type',
      text: '消息分类',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/message/type'], resolve)
      }
    }, {
      path: '/message/list',
      name: 'message_list',
      text: '消息列表',
      menu: true,
      component: function (resolve) {
        require(['admin/pages/message/list'], resolve)
      }
    }]
  }]
}]

export default routers
