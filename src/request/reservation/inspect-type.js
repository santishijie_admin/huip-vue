import service from '@/request/base'

export default {
  save (body) {
    return service.post('huipmaa/maaapi/maa/inspectType/save', body)
  },
  delete (body) {
    return service.post('huipmaa/maaapi/maa/inspectType/delete', body)
  },
  load (body) {
    return service.post('huipmaa/maaapi/maa/inspectType/load', body)
  },
  list (body) {
    return service.post('huipmaa/maaapi/maa/inspectType/list', body)
  }
}
