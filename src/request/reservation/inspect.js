import service from '@/request/base'

export default {
  save (body) {
    return service.post('huipmaa/maaapi/maa/inspect/save', body)
  },
  delete (body) {
    return service.post('huipmaa/maaapi/maa/inspect/delete', body)
  },
  load (body) {
    return service.post('huipmaa/maaapi/maa/inspect/load', body)
  },
  list (body) {
    return service.post('huipmaa/maaapi/maa/inspect/list', body)
  },
  find (body, pagination) {
    return service.post('huipmaa/maaapi/maa/inspect/find', service.paginationBody(body, pagination))
  }
}
