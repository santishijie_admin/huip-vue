import service from '@/request/base'

export default {
  save (body) {
    return service.post('huipmaa/maaapi/maa/patient/save', body)
  },
  delete (body) {
    return service.post('huipmaa/maaapi/maa/patient/delete', body)
  },
  load (body) {
    return service.post('huipmaa/maaapi/maa/patient/load', body)
  },
  list (body) {
    return service.post('huipmaa/maaapi/maa/patient/list', body)
  },
  find (body, pagination) {
    return service.post('huipmaa/maaapi/maa/patient/find', service.paginationBody(body, pagination))
  }
}
