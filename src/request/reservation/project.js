import service from '@/request/base'

export default {
  save (body) {
    return service.post('huipmaa/maaapi/maa/project/save', body)
  },
  delete (body) {
    return service.post('huipmaa/maaapi/maa/project/delete', body)
  },
  load (body) {
    return service.post('huipmaa/maaapi/maa/project/load', body)
  },
  list (body) {
    return service.post('huipmaa/maaapi/maa/project/list', body)
  },
  find (body, pagination) {
    return service.post('huipmaa/maaapi/maa/project/find', service.paginationBody(body, pagination))
  }
}
