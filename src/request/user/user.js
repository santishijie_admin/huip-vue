import service from '@/request/base'

export default {
  list (query) {
    return service({
      url: '/user/user/list',
      method: 'get',
      params: query
    })
  }
}
